from django.urls import path
from . import views


urlpatterns = [
    path('registration', views.registration, name='registration'),
    path('get_csrf', views.get_csrf, name='get_csrf'),
]