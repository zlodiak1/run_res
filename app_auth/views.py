from django.shortcuts import render
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.views.decorators.csrf import ensure_csrf_cookie


@ensure_csrf_cookie
def get_csrf(request):
    response = JsonResponse([{"cookie_set": True}], safe=False)
    return response

def registration(request):
    print('----', request.method, request.POST)
    print('----', request.POST['username'])
    username = request.POST['username']
    email = request.POST['email']
    password = request.POST['password']

    print('===', username, email, password)

    if _reg_data_exist(username, email, password):
        user = User.objects.create_user(username, email, password)
        user.save()

    response = JsonResponse([{"registration_successful": True}], safe=False)
    return response

def _reg_data_exist(username, email, password):
    is_exist = True
    if len(username.strip()) == 0: is_exist = False
    if len(email.strip()) == 0: is_exist = False
    if len(password.strip()) == 0: is_exist = False
    return is_exist
