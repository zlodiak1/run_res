from django.urls import path
from . import views


urlpatterns = [
    path('results', views.results, name='results'),
    path('result/<str:date_from>/<str:date_to>/<str:user_id>', views.result, name='result'),
]