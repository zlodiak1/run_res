from django.db import models
from django.utils import timezone

class Result(models.Model):
    user = models.ForeignKey('auth.User', on_delete=models.SET_NULL, null=True)
    period_min = models.IntegerField()
    distance_km = models.IntegerField()
    desc = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return str(self.distance_km) + 'km/' + str(self.period_min) + 'min'