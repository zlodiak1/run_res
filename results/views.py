from django.shortcuts import render
from django.utils import timezone
from django.http import JsonResponse
from django.core import serializers
import json

from .models import Result


def results(request):
    results = Result.objects.all()   
    results_serialized = serializers.serialize('json', results)
    return JsonResponse(results_serialized, safe=False)

def result(request, date_from, date_to, user_id):
    results = Result.objects.filter(
        user_id=user_id, 
        created_date__gte=date_from[0:10], 
        created_date__lte=date_to[0:10],
    )
    print(results)
    results_serialized = serializers.serialize('json', results)
    response = JsonResponse(results_serialized, safe=False)
    response['Access-Control-Allow-Origin'] = '*'
    response['Access-Control-Allow-Methods'] = 'GET'
    return response